from tensorflow import keras
import numpy as np

class IADeep:
    def __init__(self):
        self.__model = keras.models.load_model('./puissance4.keras')

    def play(self, grille):
        result = self.__model.predict(np.array([grille]))[0]
        print(result)
        max = - 100000
        column_max = 0
        for column in range(7):
            if (result[column]>max):
                max = result[column]
                column_max = column
        return column_max
    
class IAMinMax:
    def __init__(self):
        self.profondeur = 4

    def is_valid_location(self, board, col):
        return board[5][col] == 0

    def get_valid_locations(self, board):
        valid_locations = []
        for col in range(7):
            if self.is_valid_location(board, col):
                valid_locations.append(col)
        return valid_locations
    
    def winning_move(self, board, piece):
        # Check horizontal locations
        for c in range(4):
            for r in range(6):
                if board[r][c] == piece and board[r][c + 1] == piece and board[r][c + 2] == piece and board[r][c + 3] == piece:
                    return True

        # Check vertical locations
        for c in range(7):
            for r in range(3):
                if board[r][c] == piece and board[r + 1][c] == piece and board[r + 2][c] == piece and board[r + 3][c] == piece:
                    return True

        # Check positively sloped diagonals
        for c in range(4):
            for r in range(3):
                if board[r][c] == piece and board[r + 1][c + 1] == piece and board[r + 2][c + 2] == piece and board[r + 3][c + 3] == piece:
                    return True

        # Check negatively sloped diagonals
        for c in range(4):
            for r in range(3, 6):
                if board[r][c] == piece and board[r - 1][c + 1] == piece and board[r - 2][c + 2] == piece and board[r - 3][c + 3] == piece:
                    return True

        return False
    
    def is_terminal_node(self, board):
        return self.winning_move(board, 1) or self.winning_move(board, 2) or len(self.get_valid_locations(board)) == 0
    
    def evaluate_window(self, window, piece):
        score = 0
        opp_piece = 1 if piece == 2 else 2

        if window.count(piece) == 4:
            score += 1000000
        elif window.count(piece) == 3 and window.count(0) == 1:
            score += 5
        elif window.count(piece) == 2 and window.count(0) == 2:
            score += 2

        if window.count(opp_piece) == 3 and window.count(0) == 1:
            score -= 4

        return score
    
    def score_position(self, board, piece):
        score = 0

        # Score center column
        center_array = [int(i) for i in list(board[:, 7 // 2])]
        center_count = center_array.count(piece)
        score += center_count * 3

        # Score Horizontal
        for r in range(6):
            row_array = [int(i) for i in list(board[r, :])]
            for c in range(4):
                window = row_array[c:c + 4]
                score += self.evaluate_window(window, piece)

        # Score Vertical
        for c in range(7):
            col_array = [int(i) for i in list(board[:, c])]
            for r in range(3):
                window = col_array[r:r + 4]
                score += self.evaluate_window(window, piece)

        # Score positive sloped diagonal
        for r in range(3):
            for c in range(4):
                window = [board[r + i][c + i] for i in range(4)]
                score += self.evaluate_window(window, piece)

        # Score negative sloped diagonal
        for r in range(3):
            for c in range(4):
                window = [board[r + 3 - i][c + i] for i in range(4)]
                score += self.evaluate_window(window, piece)

        return score
    
    def get_next_open_row(self, board, col):
        for r in range(6):
            if board[r][col] == 0:
                return r
            
    def drop_piece(self, board, row, col, piece):
        board[row][col] = piece

    def minimax(self, grille, profondeur, maximizing_player):
        valid_locations = self.get_valid_locations(grille)
        is_terminal = self.is_terminal_node(grille)
        if profondeur == 0 or is_terminal:
            if is_terminal:
                if self.winning_move(grille, 2):
                    return (None, 1000000)
                elif self.winning_move(grille, 1):
                    return (None, -1000000)
                else:  # Game is over, no more valid moves
                    return (None, 0)
            else:  # Depth is zero
                return (None, self.score_position(grille, 2))
        if maximizing_player:
            value = -np.inf
            column = np.random.choice(valid_locations)
            for col in valid_locations:
                row = self.get_next_open_row(grille, col)
                temp_board = grille.copy()
                self.drop_piece(temp_board, row, col, 2)
                new_score = self.minimax(temp_board, profondeur - 1, False)[1]
                if new_score > value:
                    value = new_score
                    column = col
            return column, value
        else:  # Minimizing player
            value = np.inf
            column = np.random.choice(valid_locations)
            for col in valid_locations:
                row = self.get_next_open_row(grille, col)
                temp_board = grille.copy()
                self.drop_piece(temp_board, row, col, 1)
                new_score = self.minimax(temp_board, profondeur - 1, True)[1]
                if new_score < value:
                    value = new_score
                    column = col
            return column, value
    
    def play(self, grille):
        col, minimax_score = self.minimax(grille, 4, True)
        
        return col