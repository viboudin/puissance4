import pygame

class UIPuissance4:
    def __init__(self, screen) -> None:
        # Variables graphiques
        self.__grilleImageOrigin = pygame.image.load("./grillepuissance4.png")
        self.__grilleImage = self.__grilleImageOrigin
        self.__grillePos = (0,0)
        self.__resize__(screen.get_size()[0], screen.get_size()[1])
        self.__annimatedJeton = []
        self.__top = [0, 0, 0, 0, 0, 0, 0]

    def getColumn(self, x) -> int:
        pos = self.__grillePos
        dim = self.__grilleImage.get_size()

        ret = int((x-pos[0])/(dim[0]/7))
        if (x<pos[0]):
            return ret-1
        else:
            return ret
    
    def __resize__(self, width, height):
        scale = width/self.__grilleImageOrigin.get_size()[0]
        if (self.__grilleImageOrigin.get_size()[1]*scale>height):
            scale = height/self.__grilleImageOrigin.get_size()[1]

        scale *=0.8
        if scale>0.5:
            scale=0.5

        self.__grilleImage = pygame.transform.scale_by(self.__grilleImageOrigin, scale)
        self.__grillePos = ((width - self.__grilleImage.get_size()[0])/2, (height - self.__grilleImage.get_size()[1])/2)
    
    def drawGrille(self, screen, player, x = None):
        screen.fill((150,150,150))
        
        dim = self.__grilleImage.get_size()
        pos = self.__grillePos
        dimOrigin = self.__grilleImageOrigin.get_size()
        
        # Grille
        screen.blit(self.__grilleImage, self.__grillePos)

        if (x!=None):
            column = self.getColumn(x)

            # Colonne selectionnée en surbrillance
            if column>=0 and column<7:
                dim = self.__grilleImage.get_size()
                s = pygame.Surface((dim[0]/7, dim[1]))
                s.set_alpha(30)
                s.fill((255,255,255))
                screen.blit(s, (self.__grillePos[0] + column*dim[0]/7, self.__grillePos[1]))
        
                # Jeton au dessus de la colonne selectionnée
                if (player == 1):
                    color = pygame.Color(180,180,100)
                else:
                    color = pygame.Color(180, 100,100)
                pygame.draw.circle(screen, color, (pos[0] + (column+0.5)*dim[0]/7, pos[1]+dim[1] - 6.5*dim[1]/6), 0.8*dim[0]/(7*2))
            
        # Jeton en mouvement
        l = len(self.__annimatedJeton)
        i = 0
        while i<l:
            jeton = self.__annimatedJeton[i]
            jeton['speed'] -= 0.05
            jeton['y'] += jeton['speed']
            if (jeton['y']<jeton['y_end']):
                pygame.draw.circle(self.__grilleImage, jeton['color'], ((jeton['column']+0.5)*dim[0]/7, dim[1] - (jeton['y_end']+0.5)*dim[1]/6), 0.8*dim[0]/(7*2))
                pygame.draw.circle(self.__grilleImageOrigin, jeton['color'], ((jeton['column']+0.5)*dimOrigin[0]/7, dimOrigin[1] - (jeton['y_end']+0.5)*dimOrigin[1]/6), 0.8*dimOrigin[0]/(7*2))
                self.__annimatedJeton.pop(i)
                i-=1
                l-=1
            else:
                pygame.draw.circle(screen, jeton['color'], (pos[0] + (jeton['column']+0.5)*dim[0]/7, pos[1]+dim[1] - (jeton['y']+0.5)*dim[1]/6), 0.8*dim[0]/(7*2))
            i+=1
        
    
    def drawJeton(self, column, player):
        if (player == 1):
            color = '#ffde59'
        else:
            color = '#ff3131'
        self.__top[column]+=1
        self.__annimatedJeton.append({"color":color,"column": column, "y": 6, "speed":0.5, "y_end":self.__top[column]-1})
    
    def win(self):
        return True
    
    def lose(self):
        return True
    
    def draw(self):
        return True

