import pygame
import time

import puissance4
import IA
import UI

# variables
SCREEN_WIDTH = 1280 # 1280
SCREEN_HEIGHT = 720 # 720

# pygame setup
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()
menu_fond = pygame.image.load("./Puissance 4.png")
screen.blit(menu_fond, (0,0))
running = True

# UI variables
mouseX = None
state = 'menu'

while running:
    # poll for events
    # pygame.QUIT event means the user clicked X to close your window
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONUP:
            if state=='game':
                if player==1:
                    column = myUI.getColumn(event.pos[0])
                    if column>=0 and column<7:
                        code = myPuissance4.__play__(column)
                        if code==0:
                            myUI.drawJeton(column, player)
                            myUI.win()
                            state="restart"
                            print("Victoire !")
                        elif code==1:
                            myUI.drawJeton(column, player)
                        elif code==3:
                            myUI.drawJeton(column, player)
                            myUI.draw()
                            state=='restart'
                            print("Egalité")
                        player = 2
                        if code==2:
                            # Colonne pleine
                            player=1
                        t = time.time()
            elif state=="menu":
                state='game'
                myPuissance4 = puissance4.Puissance4()
                myUI = UI.UIPuissance4(screen)
                if (event.pos[0] < SCREEN_WIDTH/2):
                    myIA = IA.IADeep()
                else:
                    myIA = IA.IAMinMax()
                player = 1
            elif state=="restart":
                state='menu'
                myPuissance4 = puissance4.Puissance4()
                screen.blit(menu_fond, (0,0))
                player = 1
                
        if event.type == pygame.WINDOWSIZECHANGED:
            SCREEN_WIDTH = event.x
            SCREEN_HEIGHT = event.y
            if state!="menu":
                myUI.__resize__(SCREEN_WIDTH, SCREEN_HEIGHT)
        if event.type == pygame.MOUSEMOTION:
            if state=='game':
                mouseX = event.pos[0]

    # Update display
    if state=='game': 
        myUI.drawGrille(screen, player, mouseX)
    elif state=='restart':
        myUI.drawGrille(screen, player)
    else:
        screen.fill((128,182,255))
        screen.blit(menu_fond, ((screen.get_size()[0]-menu_fond.get_size()[0])/2,(screen.get_size()[1]-menu_fond.get_size()[1])/2))
    
    # IA turn
    if state=='game':
        if player==2 and time.time()-t>0.5:
            column = myIA.play(myPuissance4.grille)
            code = myPuissance4.__play__(column)
            if code==0:
                myUI.drawJeton(column, player)
                myUI.lose()
                state='restart'
                print("Perdu !")
            elif code==1:
                myUI.drawJeton(column, player)
            elif code==3:
                myUI.drawJeton(column, player)
                myUI.draw()
                state='restart'
                print("Egalité")
            elif code==2:
                print("L'IA s'est trompée")
                column = myPuissance4.getRandomColumn()
                code = myPuissance4.__play__(column)
                myUI.drawJeton(column, player)
                if code==0:
                    myUI.lose()
                    state='restart'
                    print("Perdu !")
                elif code==3:
                    myUI.draw()
                    state='restart'
                    print("Egalité")
            player=1


            

    # flip() the display to put your work on screen
    pygame.display.flip()

    clock.tick(60)  # limits FPS to 60

pygame.quit()