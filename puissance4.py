import numpy as np
from random import randint

class Puissance4:
    def __init__(self) -> None:
        # Variables du jeu
        self.__player = 1
        self.grille = np.array([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
            ])
        self.grilleAdversaire = np.array([
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
            ])
        self.__top = [0, 0, 0, 0, 0, 0, 0]
    
    def __play__(self, column) -> int:
        if (column>=0 and column<7):
            if self.__top[column] < 6:
                jeton = self.__player
                self.grille[self.__top[column]][column] = jeton
                self.grilleAdversaire[self.__top[column]][column] = 3 - jeton
                self.__top[column] += 1
                self.__player = 3 - self.__player # Next player
                code = self.__checkWin__(column, jeton)
                return code
            else:
                print("This column is full")
                return 2
        else:
            print("This column doesn't exist")
            return 2
    
    def __checkWin__(self, column, jeton) -> bool:
        win = self.__checkColumn__(column, jeton) or self.__checkRow__(column, jeton) or self.__checkDiag1__(column, jeton) or self.__checkdiag2__(column, jeton)
        if win:
            return 0 # Victoire
        else:
            if (any(self.__top) == 6):
                return 3 # Match nul
            else:
                return 1 # La partie continue


    def __checkRow__(self, column, jeton) -> bool:
        lenght = 1
        exploreLeft = True
        j=column
        while (exploreLeft):
            j-=1
            if j>=0:
                exploreLeft = (self.grille[self.__top[column]-1][j] == jeton)
                if exploreLeft:
                    lenght+=1
            else:
                exploreLeft = False
        
        exploreRight = True
        j=column
        while (exploreRight):
            j+=1
            if j<7:
                exploreRight = (self.grille[self.__top[column]-1][j] == jeton)
                if exploreRight:
                    lenght+=1
            else:
                exploreRight = False
        return lenght>=4

    def __checkColumn__(self, column, jeton) -> bool:
        lenght = 1
        exploreBottom = True
        i=self.__top[column]-1
        while (exploreBottom):
            i+=1
            if i<6:
                exploreBottom = (self.grille[i][column] == jeton)
                if exploreBottom:
                    lenght+=1
            else:
                exploreBottom = False
        
        exploreTop = True
        i=self.__top[column]-1
        while (exploreTop):
            i-=1
            if i>=0:
                exploreTop = (self.grille[i][column] == jeton)
                if exploreTop:
                    lenght+=1
            else:
                exploreTop = False

        return lenght>=4
    
    def __checkDiag1__(self, column, jeton) -> bool:
        # Diagonale montante de gauche à droite
        lenght = 1
        exploreLeft = True
        i=self.__top[column]-1
        j=column
        while (exploreLeft):
            i+=1
            j-=1
            if i<6 and j>=0:
                exploreLeft = (self.grille[i][j] == jeton)
                if exploreLeft:
                    lenght+=1
            else:
                exploreLeft = False
        
        exploreRight = True
        i=self.__top[column]-1
        j=column
        while (exploreRight):
            i-=1
            j+=1
            if i>=0 and j<7:
                exploreRight = (self.grille[i][j] == jeton)
                if exploreRight:
                    lenght+=1
            else:
                exploreRight = False

        return lenght>=4
    
    def __checkdiag2__(self, column, jeton) -> bool:
        # Diagonale descendante de gauche à droite
        lenght = 1
        exploreLeft = True
        i=self.__top[column]-1
        j=column
        while (exploreLeft):
            i-=1
            j-=1
            if i>=0 and j>=0:
                exploreLeft = (self.grille[i][j] == jeton)
                if exploreLeft:
                    lenght+=1
            else:
                exploreLeft = False
        
        exploreRight = True
        i=self.__top[column]-1
        j=column
        while (exploreRight):
            i+=1
            j+=1
            if i<6 and j<7:
                exploreRight = (self.grille[i][j] == jeton)
                if exploreRight:
                    lenght+=1
            else:
                exploreRight = False

        return lenght>=4
    
    def getRandomColumn(self)->int:
        n = 0
        for itop in self.__top:
            if itop<6:
                n+=1
        randN = randint(1,n)

        n=0
        i=0
        while n!=randN:
            if self.__top[i]<6:
                n+=1
            i+=1
        return i-1
